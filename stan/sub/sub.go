// Copyright 2016-2019 The NATS Authors
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package sub

import (
	"bytes"
	"fmt"
	nats "github.com/nats-io/nats.go"
	"github.com/nats-io/stan.go"
	"github.com/nats-io/stan.go/pb"
	"github.com/spf13/viper"
	"log"
	"net/http"
	"os"
	"os/signal"
	"sync/atomic"
)

func receivedMsd(m *stan.Msg, i uint32) {
	url := "http://localhost:8080/handle"
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(m.Data))
	if err != nil {
		log.Fatalln("Bad post request!")
	}
	defer resp.Body.Close()

	fmt.Println("[{", i, "}]", "Received:", string(m.Data)) // prints the object
}

var (
	clusterID, clientID string = "test-cluster", "stan-sub"
	URL                 string = stan.DefaultNatsURL
	qgroup              string = ""
	unsubscribe         bool   = false
	durable             string = ""
)

func Subscribe() {
	subj := viper.GetString("channel")
	log.Printf(subj)
	opts := []nats.Option{nats.Name("Subscriber")}

	nc, err := nats.Connect(URL, opts...)
	if err != nil {
		log.Fatal(err)
	}
	defer nc.Close()

	sc, err := stan.Connect(clusterID, clientID, stan.NatsConn(nc),
		stan.SetConnectionLostHandler(func(_ stan.Conn, reason error) {
			log.Fatalf("Connection lost, reason: %v", reason)
		}))
	if err != nil {
		log.Fatalf("Can't connect: %v.\nMake sure a NATS Streaming server is running at: %s", err, URL)
	}
	log.Printf("Connected to %s clusterID: [%s] clientID: [%s]\n", URL, clusterID, clientID)

	startOpt := stan.StartAt(pb.StartPosition_NewOnly)

	var i uint32 = 0
	mcb := func(msg *stan.Msg) {
		atomic.AddUint32(&i, 1)
		receivedMsd(msg, i)
	}

	sub, err := sc.QueueSubscribe(subj, qgroup, mcb, startOpt, stan.DurableName(durable))
	if err != nil {
		err := sc.Close()
		if err != nil {
			return
		}

		log.Fatal(err)
	}

	log.Printf("Listening on [%s], clientID=[%s], qgroup=[%s] durable=[%s]\n", subj, clientID, qgroup, durable)

	// Wait for a SIGINT (perhaps triggered by user with CTRL-C)
	// Run cleanup when signal is received
	signalChan := make(chan os.Signal, 1)
	cleanupDone := make(chan bool)
	signal.Notify(signalChan, os.Interrupt)
	go func() {
		for range signalChan {
			fmt.Printf("\nReceived an interrupt, unsubscribing and closing connection...\n\n")
			// Do not unsubscribe a durable on exit, except if asked to.
			if durable == "" || unsubscribe {
				err := sub.Unsubscribe()
				if err != nil {
					return
				}
			}
			err := sc.Close()
			if err != nil {
				return
			}
			cleanupDone <- true
		}
	}()
	<-cleanupDone
}
