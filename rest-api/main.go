package main

import (
	"github.com/joho/godotenv"
	"github.com/spf13/viper"
	"log"
	"service/handler"
	"service/postgresdb"
	"service/server"
	"service/stan/sub"
)

func init() {
	viper.AddConfigPath("configs")
	err := viper.ReadInConfig()
	if err != nil {
		log.Fatalln("Bad ReadInConfig", err)
	}

	err = godotenv.Load(".env")
	if err != nil {
		log.Fatalln("Bad godotenv", err)
	}
}

func main() {

	h := handler.Hand{postgresdb.InitDB(), map[interface{}]string{}}
	s := server.InitServer(&h)
	h.SyncCache()

	go sub.Subscribe()

	err := s.Engine.Run("localhost:" + viper.GetString("port"))
	if err != nil {
		log.Fatalln("Run error!")
	}

}
