# GoRestAPI


## Description
Simple RESTful API application that emulation shop, there you can add, delete and search some products.


Data is transmitted using a nats-streaming service (stan), which connects to an http-service.


## Technologies
Postgresql, nats, nats-streaming, docker, make, http
